%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.09.2021
%%% @doc IMAP adapter application.
%%%   Application could be setup by application:set_env:
%%%      log_destination
%%%          descriptor to define log file folder and prefix.
%%%          Default: {'env','imaplib'}
%%%      temp_folder
%%%          folder where temp files of messages would be located
%%%          Default: "temp/messages/imap"

-module(imaplib).
-author('Peter <tbotc@yandex.ru>').

-behaviour(application).

% application
-export([start/0,
         stop/0]).

% application
-export([start/2,
         stop/1]).

% api
-export([
    recv/1,
    parse/1,
    clean/1,
    list/1
]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public
%% ====================================================================

%% -----------------------------------------------------
%% LOADER FACADE
%% -----------------------------------------------------

%% -------------------------------------
%% Starts application
%% -------------------------------------
start() ->
    case application:ensure_all_started(?APP, permanent) of
        {ok, _Started} -> ok;
        Error -> Error
    end.

%% -------------------------------------
%% Stops application
%% -------------------------------------
stop() -> application:stop(?APP).

%% ===================================================================
%% API functions
%% ===================================================================

%% ---------------------------------------
%% Make receive operation
%% Connect, login, select folder, and read next portion of messages from previous uid_validity/uid_next up to current uid_next
%% Return current UIDValidity and next UIDNext corresponding last returned message (not box's current value)
%% You should call this method until it would result with [] as list of messages.
%% Note! Id and UID are different values. UID is permanent for message, Id is sequence from 1... of existing messages in box.
%% ---------------------------------------
%%    #{
%%        server => "imap.yandex.ru",
%%        ssl => true | false,
%%          % default true
%%        port => 993,
%%        login => "...",
%%        pwd => "...",
%%        folder => "INBOX",
%%        uid_validity => 1330152173,
%%          % on first time it could be 0. Returned UidValidity from previous call.
%%        uid_next => 15234,
%%          % returned uid_next from previous call to get next message(s).
%%        load_existing => false,
%%          % default false. If previous UidValidity differs from current box's UidValidity, then it would result by downloading limited messages from 1.
%%        delete_after_read => false,
%%          % default false. If message should be marked as \Deleted after receive. At the end of whole operation box would be expunged.
%%        limit => 0
%%          % limit of messages to receive. Recommended to select non-zero value. This would avoid total error with next repeat receive all messages at the same time.
%%        % id => 1660
%%          % if Id is set then only selected message is to be received. Same could be achieved by limit=1,uid_next=UID
%%    }.
%% ---------------------------------------
-spec recv(Setup::map())
      -> {ok,{UidValidity::integer(),UidNext::integer()},[{UID::integer(),Rfc822Filepath::string()}]} | {error,Reason::term()}.
%% ---------------------------------------
recv(Setup) when is_map(Setup) ->
    ?Recv:recv(Setup).

%% ---------------------------------------
%% Parse rfc822 message from file into Map with values
%% Create attachments folder and fill it with files if found
%% All links are added into map.
%% Returned map contains:
%%   #{
%%      from :: binary(),
%%      to :: [binary()],
%%      cc :: [binary()],
%%      subject :: binary(),
%%      references :: [binary()],
%%      native_id :: binary(),
%%      reply_to :: binary(),
%%      size :: integer(),
%%      text :: binary(), optional
%%      html :: Filepath::string(), optional
%%      attachments :: [Filepath::string()], optional
%%      inlines :: [Filepath::string()], optional
%%      others :: [Filepath::string()] optional
%%    }
%% ---------------------------------------
-spec parse(Filepath::string())
      -> {ok,Map::map()} | {error,Reason::term()}.
%% ---------------------------------------
parse(Filepath) when is_list(Filepath) ->
    ?Parse:parse(Filepath).

%% ---------------------------------------
%% Cleans temp files and folders of received message
%% ---------------------------------------
clean(Filepath) when is_list(Filepath) ->
    case ?BLfilelib:is_regular(Filepath) of
        true -> ?BLfile:delete(Filepath);
        false -> ok
    end,
    Extension = filename:extension(Filepath),
    Dirpath = string:left(Filepath,length(Filepath) - length(Extension)) ++ ".files",
    case ?BLfilelib:is_dir(Dirpath) of
        true -> ?BU:directory_delete(Dirpath);
        false -> ok
    end.

%% ---------------------------------------
%% Cleans temp files and folders of received message
%% ---------------------------------------
list(Setup) ->
    ?List:list(Setup).

%% ===================================================================
%% Application callback functions
%% ===================================================================

%% -------------------------------------
%% @doc Starts application
%% -------------------------------------
start(_, _) ->
    setup_start_ts(),
    setup_dependencies(),
    ensure_deps_started(),
    ?OUT('$info',"~ts. ~p start", [?APP, self()]),
    ?SUPV:start_link(undefined).

%% -------------------------------------
%% @doc Stops application.
%% -------------------------------------
stop(State) ->
    ?OUT('$info',"~ts: Application stopped (~120p)", [?APP, State]),
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% --------------------------------------
%% @private
%% setup start_utc to applicaiton's env for monitor purposes
%% --------------------------------------
setup_start_ts() ->
    application:set_env(?APP,start_utc,?BU:timestamp()).

%% --------------------------------------
%% @private
%% setup opts of dependency apps
%% --------------------------------------
setup_dependencies() -> ok.

%% --------------------------------------
%% @private
%% starts deps applications, that are not linked to app, and to ensure
%% --------------------------------------
ensure_deps_started() ->
    {ok,_} = application:ensure_all_started(?BASICLIB, permanent),
    {ok,_} = application:ensure_all_started(mimetypes, permanent),
    {ok,_} = application:ensure_all_started(erlimap, permanent),
    {ok,_} = application:ensure_all_started(gen_smtp, permanent),
    {ok,_} = application:ensure_all_started(imap_utf7, permanent).