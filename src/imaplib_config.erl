%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.09.2021
%%% @doc

-module(imaplib_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    log_destination/1,
    temp_folder/0
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% log_destination
log_destination(Default) ->
    ?BU:get_env(?APP, 'log_destination', Default).

%% log_destination
temp_folder() ->
    ?BU:get_env(?APP, 'temp_folder', "temp/messages/imap").

%% ====================================================================
%% Internal functions
%% ====================================================================


