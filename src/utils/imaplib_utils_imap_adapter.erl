%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.09.2021
%%% @doc

-module(imaplib_utils_imap_adapter).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    connect/5,
    disconnect/1,
    list_folders/1,
    select_folder/2,
    search_unread/2,
    search_uid/2,
    fetch_uid/2,
    fetch_header/2,
    fetch_mime_parts/2,
    fetch_body_part/3,
    fetch_message_stream/4,
    store_flags/3,
    expunge/1
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------------------------
%% Connect and login to IMAP server
%% NOTE! Don't forget to monitor Pid.
%% -------------------------------------------
-spec connect(Host::string(), Port::non_neg_integer(), Ssl::boolean(), Login::string(), Pwd::string())
      -> {ok,Pid::pid()} | {error,Reason::term()}.
%% -------------------------------------------
connect(Host,Port,Ssl,Login,Pwd) ->
    Mode = case Ssl of
               true -> ssl;
               false -> tcp
           end,
    imap:open_account(Mode,Host,Port,Login,Pwd).

%% -------------------------------------------
%% Close connection to IMAP server
%% -------------------------------------------
-spec disconnect(Imap::pid())
      -> {ok,Pid::pid()} | {error,Reason::term()}.
%% -------------------------------------------
disconnect(Imap) ->
    imap:close_account(Imap).

%% -------------------------------------------
%% List existing folders.
%% TODO: implement in erlimap
%% -------------------------------------------
-spec list_folders(Imap::pid())
      -> {ok,Map::map()} | {error,Reason::term()}.
%% -------------------------------------------
list_folders(Imap) ->
    parse_list_response(
        imap:list(Imap, ["\"\" *"])).

%% -------------------------------------------
%% Selects folder as current.
%% Server stashes folder's values for next requests unchanged until another select is called in current connection.
%% Returns ok if folder exists and selected, also returns map with
%%    uid_validity::non_neg_integer(), (to check box's same flow as last messages fetched)
%%    uid_next :: non_neg_integer(),
%%    exists :: non_neg_integer(), (how many messages are in box)
%%    recent :: non_neg_integer(),
%%    unseen :: non_neg_integer(),
%%    flags :: string(),
%%    permanentflags :: string(),
%%    mode :: r | rw | w
%% -------------------------------------------
-spec select_folder(Imap::pid(), Folder::string())
      -> {ok,Map::map()} | {error,Reason::string()}.
%% -------------------------------------------
select_folder(Imap,Folder) ->
    Folder1 = ?BU:to_list(imap_utf7:encode(?BU:to_binary(Folder))),
    parse_select_response(
        imap:select(Imap, Folder1)).

%% -------------------------------------------
%% Return ids of unread messages from folder by UIDNext, UIDLast.
%% Process:
%%    1. Store UIDNext, UIDValidity, EXISTS after select_folder as current values.
%%    2. Check if new UIDValidity is the same with previous UIDValidity.
%%    2a. If false, then fetch all messages from 0 up to EXISTS value.
%%    2b. If true, then
%%       2b.1. Check if previous UIDNext == current UIDValidity
%%       2b.1a. If true then exit with no messages
%%       2b.1b. If false then
%%          2b.1b.1. Search for new messages from previous UIDNext
%%          2b.1b.2. Fetch all messages from returned list of ids.
%% -------------------------------------------

%% -------------------------------------------
%% Returns unread messages by prev uidnext.
%% NOTE! To guarantee correct values you should provide that
%%   a) Current UIDValidity is the same as previous UIDValidity
%%   b) Current UIDNext is higher than previous UIDNext
%% -------------------------------------------
-spec search_unread(Imap::pid(), PrevUIDNext::integer())
      -> {ok,Ids::[non_neg_integer()]} | {error,Reason::term()}.
%% -------------------------------------------
search_unread(Imap, PrevUIDNext) when is_integer(PrevUIDNext) ->
    parse_search_response(
        imap:search(Imap,[?BU:str("UID ~p:* UNDELETED",[PrevUIDNext])])).

%% -------------------------------------------
%% Returns unread messages by prev uidnext.
%% NOTE! To guarantee correct values you should provide that
%%   a) Current UIDValidity is the same as previous UIDValidity
%%   b) Current UIDNext is higher than previous UIDNext
%% -------------------------------------------
-spec search_uid(Imap::pid(), UID::integer())
      -> {ok,Ids::[non_neg_integer()]} | {error,Reason::term()}.
%% -------------------------------------------
search_uid(Imap, UID) when is_integer(UID) ->
    parse_search_response(
        imap:search(Imap,[?BU:str("UID ~p UNDELETED",[UID])])).

%% -------------------------------------------
%% Returns header of message by it's temporary id
%% -------------------------------------------
-spec fetch_uid(Imap::pid(), Id::integer())
      -> {ok,UID::non_neg_integer()} | {error,Reason::term()}.
%% -------------------------------------------
fetch_uid(Imap,Id) when is_integer(Id) ->
    case parse_fetch_response(
             imap:fetch(Imap, ?BU:to_list(Id), "UID")) of
        {ok,[Res]} ->
            [_,UID] = binary:split(?BU:to_binary(string:trim(Res,both,"()")), <<" ">>),
            {ok,?BU:to_int(UID)};
        {ok,[]} -> {error,not_found};
        {error,_}=Err -> Err
    end.

%% -------------------------------------------
%% Returns header of message by it's temporary id
%% -------------------------------------------
-spec fetch_header(Imap::pid(), Id::integer())
      -> {ok,Headers::[string()]} | {error,Reason::term()}.
%% -------------------------------------------
fetch_header(Imap,Id) ->
    parse_fetch_response(
        imap:fetch(Imap, ?BU:to_list(Id), "BODY[HEADER]")).

%% -------------------------------------------
%% Returns mime parts description by it's temporary id
%% -------------------------------------------
-spec fetch_mime_parts(Imap::pid(), Id::integer())
      -> {ok,Parts::[string()]} | {error,Reason::term()}.
%% -------------------------------------------
fetch_mime_parts(Imap,Id) ->
    parse_fetch_response(
        imap:fetch(Imap, ?BU:to_list(Id), "BODY")).


%% -------------------------------------------
%% Returns mime part data by it's temporary id
%% -------------------------------------------
-spec fetch_body_part(Imap::pid(), Id::integer(), PartNo::non_neg_integer())
      -> {ok,Data::[string()]} | {error,Reason::term()}.
%% -------------------------------------------
fetch_body_part(Imap,Id,PartNo) ->
    parse_fetch_response(
        imap:fetch(Imap, ?BU:to_list(Id), ?BU:str("BODY[~p]",[PartNo]))).

%% -------------------------------------------
%% Returns whole message by it's temporary id into stream (StreamFun)
%% Return Tag of command and starts to send received data into StreamFun.
%% -------------------------------------------
-spec fetch_message_stream(Imap::pid(), Id::integer(), KeepUnseen::boolean(), StreamFun::function())
      -> {ok,Tag::string()} | {error,Reason::term()}.
%% -------------------------------------------
fetch_message_stream(Imap,Id,KeepUnseen,StreamFun) ->
    case KeepUnseen of
        false -> imap:fetch(Imap, ?BU:to_list(Id), "BODY[]", 5000, StreamFun);
        true -> imap:fetch(Imap, ?BU:to_list(Id), "BODY.PEEK[]", 5000, StreamFun)
    end.

%% -------------------------------------------
%% Store flag to message
%% -------------------------------------------
-spec store_flags(Imap::pid(), SequenceSet::integer() | string(), Flags::string())
      -> {ok,Data::binary()} | {error,Reason::term()}.
%% -------------------------------------------
store_flags(Imap,SequenceSet,Flags) ->
    imap:store(Imap, ?BU:to_list(SequenceSet), "+flags", ?BU:to_list(Flags)).

%% -------------------------------------------
%% Delete messages marked as DELETED
%% -------------------------------------------
-spec expunge(Imap::pid())
      -> {ok,Data::binary()} | {error,Reason::term()}.
%% -------------------------------------------
expunge(Imap) ->
    imap:expunge(Imap).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------------
parse_select_response({ok,Response}) when is_list(Response) ->
    lists:foldl(fun({response,untagged,V1,V2}, {ok,Acc}) -> {ok, maps:merge(Acc,parse_line(V1,V2))};
                   ({response,_Tag,"OK",{Opts,"SELECT Completed."}}, {ok,Acc}) -> {ok,Acc#{opts => Opts}};
                   ({response,_Tag,"NO",Reason}, _) -> {error,Reason};
                   ({response,_,_,_}, Acc) -> Acc
                end, {ok,#{}}, Response).

%% @private
parse_line("FLAGS", Value) -> #{flags => Value};
parse_line("EXISTS", Value) -> #{exists => ?BU:to_int(Value)};
parse_line("RECENT", Value) -> #{recent => ?BU:to_int(Value)};
parse_line("OK", {["PERMANENTFLAGS"|Values],_Limited}) -> #{permanentflags => [string:trim(Value, both, ")(") || Value <- Values]};
parse_line("OK", {["UNSEEN",Value],_}) -> #{unseen => ?BU:to_int(Value)};
parse_line("OK", {["UIDNEXT",Value],_}) -> #{uid_next => ?BU:to_int(Value)};
parse_line("OK", {["UIDVALIDITY",Value],_}) -> #{uid_validity => ?BU:to_int(Value)};
parse_line(_, _) -> #{}.

%% ----------------------------
parse_search_response({ok,Response}) when is_list(Response) ->
    lists:foldl(fun({response,untagged,"SEARCH",Ids}, {ok,[]}) -> {ok,lists:map(fun(Id) -> {id,?BU:to_int(Id)} end, Ids)};
                   ({response,_Tag,"OK",{_Opts,"SEARCH Completed."}}, {ok,Acc}) -> {ok,lists:reverse(Acc)};
                   ({response,_Tag,"NO",Reason}, _) -> {error,Reason};
                   ({response,_Tag,"BAD",Reason}, _) -> {error,Reason};
                   ({response,_,_,_}, Acc) -> Acc
                end, {ok,[]}, Response).

%% ----------------------------
parse_fetch_response({ok,Response}) when is_list(Response) ->
    lists:foldl(fun({response,untagged,"FETCH",{_Id,Line}}, {ok,Acc}) -> {ok,[Line|Acc]};
                   ({response,untagged,[],[]}, {ok,Acc}) -> {ok,[""|Acc]};
                   ({response,untagged,Line,[]}, {ok,Acc}) -> {ok,[Line|Acc]};
                   ({response,_Tag,"OK",{_Opts,"FETCH Completed."}}, {ok,Acc}) -> {ok,lists:reverse(Acc)};
                   ({response,_Tag,"NO",Reason}, _) -> {error,Reason};
                   ({response,_Tag,"BAD",Reason}, _) -> {error,Reason};
                   ({response,_,_,_}, Acc) -> Acc
                end, {ok,[]}, Response).

%% ----------------------------
parse_list_response({ok,Response}) when is_list(Response) ->
    lists:foldl(fun({response,untagged,"LIST",{_Id,Line}}, {ok,Acc}) -> {ok,[Line|Acc]};
                   ({response,untagged,Line,[]}, {ok,Acc}) ->
                       BLine = ?BU:to_binary(Line),
                       case re:run(BLine,<<"\\((.*)\\).*\s([^ ]+)$">>) of
                           nomatch -> {ok,Acc};
                           {match,[{_,_},{From1,Len1},{From2,Len2}]} ->
                               Name0 = string:trim(binary:part(BLine,From2,Len2),both,[$"]),
                               Name = imap_utf7:decode(Name0),
                               Flags = binary:split(binary:part(BLine,From1,Len1),[<<" ">>,<<"\t">>],[global]),
                               {ok, [{Name, Flags} | Acc]}
                       end;
                   ({response,_Tag,"OK",{_Opts,"LIST Completed."}}, {ok,Acc}) -> {ok,lists:reverse(Acc)};
                   ({response,_Tag,"NO",Reason}, _) -> {error,Reason};
                   ({response,_Tag,"BAD",Reason}, _) -> {error,Reason};
                   ({response,_,_,_}, Acc) -> Acc
                end, {ok,[]}, Response).