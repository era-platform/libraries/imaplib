%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.09.2021
%%% @doc

-module(imaplib_utils_list).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([list/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------------------------
%% Connect to IMAP server, login,
%% Return list of folders
%%   Setup example:
%%    #{
%%        server => "imap.yandex.ru",
%%        port => 993,
%%        login => "...",
%%        pwd => "...",
%%    }.
%% -------------------------------------------
-spec list(Setup::map())
      -> {ok,Folders::[{Name::string(),Flags::[string()]}]} | {error,Reason::term()}.
%% -------------------------------------------
list(Setup) ->
    % connect to server, login
    [Server,Port,Login,Pwd] = ?BU:maps_get([server,port,login,pwd],Setup),
    Ssl = ?BU:to_bool(?BU:get_by_key(ssl,Setup,true)),
    case ?Adapter:connect(Server,Port,Ssl,Login,Pwd) of
        {ok,Pid} ->
            ?LOG('$trace',"Connected and logged in: ~p",[Pid]),
            Res = list_1(Setup,Pid),
            ?Adapter:disconnect(Pid),
            Res;
        {error,_}=Err -> Err
    end.

%% @private
list_1(_Setup,Pid) ->
    case ?Adapter:list_folders(Pid) of
        {ok,List} ->
            ?LOG('$trace',"List: ~p",[List]),
            {ok,List};
        {error,_}=Err -> Err
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================