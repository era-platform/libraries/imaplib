%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.09.2021
%%% @doc

-module(imaplib_utils_recv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([recv/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------------------------
%% Connect to IMAP server, login, select folder.
%% Receive new messages (from previous uid_next with same uid_validity).
%% Also could receive selected message only.
%%   Setup example:
%%    #{
%%        server => "imap.yandex.ru",
%%        port => 993,
%%        login => "...",
%%        pwd => "...",
%%        folder => "INBOX",
%%        key => <<"12341234-1234-1234-1234-123412341234">>, % optional
%%        uid_validity => 1330152173,
%%        uid_next => 15234,
%%        load_existing => false, % default false
%%        delete_after_read => false, % default false
%%        % limit => 100000
%%        % id => 1660
%%    }.
%% -------------------------------------------
-spec recv(Setup::map())
      -> {ok,{UidValidity::integer(),UidNext::integer()},Messages::[{UID::integer(),Path::string()}]} | {error,Reason::term()}.
%% -------------------------------------------
recv(Setup) ->
    % connect to server, login
    [Server,Port,Login,Pwd] = ?BU:maps_get([server,port,login,pwd],Setup),
    Ssl = ?BU:to_bool(?BU:get_by_key(ssl,Setup,true)),
    case ?Adapter:connect(Server,Port,Ssl,Login,Pwd) of
        {ok,Pid} ->
            ?LOG('$trace',"Connected and logged in: ~p",[Pid]),
            Res = recv_1(Setup,Pid),
            case maps:get(delete_after_read,Setup,false) of
                true -> ?Adapter:expunge(Pid);
                false -> ok
            end,
            ?Adapter:disconnect(Pid),
            Res;
        {error,_}=Err -> Err
    end.

%% @private
%% Select folder
recv_1(Setup,Pid) ->
    % select folder
    Folder = maps:get(folder,Setup),
    case ?Adapter:select_folder(Pid,Folder) of
        {ok,Map} ->
            ?LOG('$trace',"Selected: ~p",[Map]),
            recv_2(Setup,Pid,Map);
        {error,_}=Err -> Err
    end.

%% @private
%% Check if direct message fetching
recv_2(Setup,Pid,Map) ->
    case maps:get(id, Setup, undefined) of
        undefined -> recv_3(Setup,Pid,Map);
        Id when is_integer(Id) ->
            UidValidity = maps:get(uid_validity,Setup),
            case ?Adapter:fetch_uid(Pid,Id) of
                {ok,UID} ->
                    case fetch(Pid,Setup,fetch_tuple(Setup,UidValidity),[{undefined,Id}]) of
                        {error,_}=Err -> Err;
                        Files -> {ok,{UidValidity,UID},Files}
                    end;
                {error,_}=Err -> Err
            end end.

%% @private
%% Check uid_validity and uid_next. Generate message ids to recv
recv_3(Setup,Pid,Map) ->
    Limit = maps:get(limit,Setup,0),
    % check unread
    case {maps:get(uid_validity,Setup), maps:get(uid_validity,Map)} of
        {UidValidity,UidValidity} ->
            case {maps:get(uid_next,Setup), maps:get(uid_next,Map)} of
                {UidNext,UidNext} -> {ok,{UidValidity,UidNext},[]};
                {PrevUidNext,CurrUidNext} ->
                    ?LOG('$trace',"Search from ~p to ~p",[PrevUidNext,CurrUidNext]),
                    case ?Adapter:search_unread(Pid,PrevUidNext) of
                        {ok,[]} ->
                            ?LOG('$trace',"Search result: []",[]),
                            {ok,{UidValidity,CurrUidNext},[]};
                        {ok,Ids} when Limit<length(Ids), Limit>0 ->
                            ?LOG('$trace',"Search result: ~200tp",[Ids]),
                            ?LOG('$trace',"Fetching new limited",[]),
                            Ids1 = lists:sort([Id || {id,Id} <- Ids]),
                            recv_4_first(Setup,Pid,Map,{UidValidity,CurrUidNext},Ids1,[]);
                        {ok,Ids} ->
                            ?LOG('$trace',"Search result: ~200tp",[Ids]),
                            ?LOG('$trace',"Fetching new",[]),
                            Ids1 = lists:sort([Id || {id,Id} <- Ids]),
                            Messages = fetch(Pid,Setup,fetch_tuple(Setup,Map),[{undefined,Id} || Id <- Ids1]),
                            auto_delete(Pid,Ids1,Setup),
                            {ok,{UidValidity,CurrUidNext},Messages}
                    end
            end;
        {_,UidValidity} ->
            UidNext = maps:get(uid_next,Map),
            Exists = maps:get(exists,Map),
            case maps:get(load_existing,Setup,false) of
                false ->
                    ?LOG('$trace',"Skip existing from ~p to ~p",[1,Exists]),
                    {ok,{UidValidity,UidNext},[]};
                true when Limit>0 ->
                    ?LOG('$trace',"Fetching all limited",[]),
                    recv_4_first(Setup,Pid,Map,{UidValidity,1},lists:seq(1,Exists),[]);
                true ->
                    ?LOG('$trace',"Fetching all from ~p to ~p",[1,Exists]),
                    Messages = fetch(Pid,Setup,fetch_tuple(Setup,Map),[{undefined,Id} || Id <- lists:seq(1,Exists)]),
                    auto_delete(Pid,lists:seq(1,Exists),Setup),
                    {ok,{UidValidity,UidNext},Messages}
            end
    end.

%% @private
%% Fetch only first new
recv_4_first(_Setup,_Pid,_Map,{UidValidity,UidNext},[],Acc) -> {ok,{UidValidity,UidNext},Acc};
recv_4_first(Setup,Pid,Map,{UidValidity,UidNext},[Id|Rest],Acc) ->
    Limit = maps:get(limit,Setup,0),
    case ?Adapter:fetch_uid(Pid,Id) of
        {ok,UID} ->
            case ?Adapter:search_uid(Pid,UID) of
                {ok,[{id,Id}]} ->
                    Messages = fetch(Pid,Setup,fetch_tuple(Setup,Map),[{UID,Id}]),
                    auto_delete(Pid,[Id],Setup),
                    case length(Acc) of
                        Len when Len >= Limit-1 -> {ok,{UidValidity,UID+1},Acc++Messages};
                        _ -> recv_4_first(Setup,Pid,Map,{UidValidity,UID+1},Rest,Acc++Messages)
                    end;
                {error,_} ->
                    recv_4_first(Setup,Pid,Map,{UidValidity,UidNext},Rest,Acc)
            end;
        {error,not_found} ->
            recv_4_first(Setup,Pid,Map,{UidValidity,UidNext},Rest,Acc);
        {error,_}=Err -> Err
    end.

%% @private
fetch_tuple(Setup,Map) when is_map(Map) ->
    fetch_tuple(Setup,maps:get(uid_validity,Map));
fetch_tuple(Setup,UidValidity) when is_integer(UidValidity) ->
    {maps:get(server,Setup), maps:get(login,Setup),maps:get(folder,Setup),UidValidity}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% --------------------------------
-spec fetch(Pid::pid(),Setup::map(),{Address::string(),Login::string(),BoxName::string(),UidValidity::integer()},Ids::[{UID::integer() | undefined, Id::integer()}])
      -> [{UID::integer(),Filepath::string()}] | {error,Reason::term()}.
%% --------------------------------
fetch(Pid,Setup,{Address,Login,BoxName,UidValidity},Ids) ->
    Self = self(),
    Ref = make_ref(),
    Dirpath = case maps:get(key,Setup,undefined) of
                  undefined -> filename:join([get_temp_folder(),Address,Login,BoxName,?BU:to_list(UidValidity)]);
                  Key ->
                      % distinguish disk folders for possible several separate representations of same email account
                      filename:join([get_temp_folder(),Address,Login,BoxName,?BU:to_list(Key),?BU:to_list(UidValidity)])
              end,
    ok = ?BLfilelib:ensure_dir(filename:join(Dirpath,"x")),
    MonRef = erlang:monitor(process,Pid),
    Res = lists:foldl(fun(_,{error,_}=Err) -> Err;
                         ({UID0,Id},Acc) ->
                                case UID0 of
                                    undefined ->
                                        case ?Adapter:fetch_uid(Pid,Id) of
                                            {ok,UID} ->
                                                fetch_x(Pid,Setup,{UID,Id},{Dirpath,Self,Ref,MonRef},Acc);
                                            {error,not_found} -> undefined;
                                            {error,_} -> undefined
                                        end;
                                    UID when is_integer(UID) ->
                                        fetch_x(Pid,Setup,{UID,Id},{Dirpath,Self,Ref,MonRef},Acc)
                                end end, [], Ids),
    erlang:demonitor(MonRef),
    Res.

%% @private
fetch_x(Pid,Setup,{UID,Id},{Dirpath,Self,Ref,MonRef},Acc) ->
    %Filename = ?BU:str("msg_~p_~p.msg", [?BU:timestamp(),10000 + ?BU:random(90000)]),
    Filename = ?BU:str("~p.msg", [UID]),
    Filepath = filename:join(Dirpath,Filename),
    case ?BLfile:open(Filepath, [write, {delayed_write,65536,1000}]) of
        {error,_}=Err -> Err;
        {ok,WF} ->
            FunClose = fun() -> ?BLfile:close(WF) end,
            FunDelete = fun() -> FunClose(), ?BLfile:delete(Filepath) end,
            try StreamFun = fun({response,untagged,"FETCH",{_IdStr,_Descr}}) -> ok;
                               ({response,untagged,A,[]}) -> Self ! {data,Ref,?BU:to_binary(A)};
                               ({response,Tag,"OK",{[],_}}) -> Self ! {result,Ref,Tag,ok};
                               ({response,Tag,_,{["CLIENTBUG"],Reason}}) -> Self ! {result,Ref,Tag,{error,Reason}};
                               ({response,Tag,"NO",Reason}) -> Self ! {result,Ref,Tag,{error,Reason}};
                               ({response,Tag,"BAD",Reason}) -> Self ! {result,Ref,Tag,{error,Reason}}
                            end,
            KeepUnseen = maps:get(keep_unseen,Setup,false),
            ?LOG('$trace',"Fetching ~p...",[Id]),
            case ?Adapter:fetch_message_stream(Pid,Id,KeepUnseen,StreamFun) of
                {ok,Tag} ->
                    F = fun F(State) ->
                                receive
                                    {'DOWN',MonRef,process,Pid,Reason} ->
                                        FunDelete(),
                                        {error,{process_down,Reason}};
                                    {result,Ref,Tag,{error,_}=Err} ->
                                        FunDelete(),
                                        Err;
                                    {result,Ref,Tag,ok} ->
                                        FunClose(),
                                        timer:sleep(10),
                                        [{UID,Filepath}|Acc];
                                    {data,Ref,Data} when State==undefined ->
                                        F(Data);
                                    {data,Ref,Data} ->
                                        ?BLfile:write(WF,State),
                                        ?BLfile:write(WF,<<"\r\n">>),
                                        F(Data)
                                end end,
                    F(undefined);
                {error,_}=Err ->
                    FunDelete(),
                    Err
            end
            catch _:R ->
                FunDelete(),
                {error,R}
            end
    end.

%%
get_temp_folder() ->
    %"/tmp/messages".
    %filename:join([?ENVCFG:get_local_path(), ?ENVNAMING:get_subpath_node_temp(), "messages"]).
    ?CFG:temp_folder().

%%
auto_delete(Pid,Ids,Setup) ->
    case maps:get(delete_after_read,Setup,false) of
        false -> ok;
        true -> delete(Pid,Ids)
    end.

%%
delete(_,[]) -> ok;
delete(Pid,[_|_]=Ids) ->
    lists:map(fun(Id) -> delete(Pid,Id) end, Ids);
delete(Pid,Id) when is_integer(Id) ->
    ?Adapter:store_flags(Pid,?BU:to_list(Id),"Deleted").