%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.09.2021
%%% @doc

-module(imaplib_utils_sample).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([setup/0, recv/1, parse/1, finalize/1]).
-export([test/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

setup() ->
    #{
        server => "imap.yandex.ru",
        port => 993,
        login => "...",
        pwd => "...",
        folder => "INBOX",
        uid_validity => 1330152173,
        uid_next => 15234,
        load_existing => false, % default false
        delete_after_read => false, % default false
        limit => 100000
        % id => 1660
    }.

recv(Setup) ->
    ?Recv:recv(Setup).

parse(Filepath) ->
    ?Parse:parse(Filepath).

finalize(Filepath) ->
    ?BLfile:delete(Filepath),
    %
    DirPath = filename:dirname(Filepath),
    Basename = filename:basename(Filepath),
    Extension = filename:extension(Filepath),
    SubdirName = string:left(Basename,length(Basename) - length(Extension)),
    TmpDir = filename:join(DirPath, SubdirName),
    ?BU:directory_delete(TmpDir).

%% ----------------------
test(Setup) ->
    case recv(Setup) of
        {error,_}=Err -> Err;
        {ok,{UidValidity,UidNext},Files} ->
            Res = lists:foldl(fun(_,{error,_}=Err) -> Err;
                                 ({UID,Filepath},{ok,Acc}) ->
                                        case parse(Filepath) of
                                            {error,_}=Err -> Err;
                                            {ok,Map} ->
                                                {ok,[Map#{uid => UID, validity => UidValidity} | Acc]}
                                        end end, {ok,[]}, Files),
            case Res of
                {error,_}=Err -> Err;
                {ok,Items} ->
                    %lists:foldl(fun(Item) -> build_message(Item) end, Items),
                    {ok,{UidValidity,UidNext},Items}
            end end.

%% ====================================================================
%% Internal functions
%% ====================================================================