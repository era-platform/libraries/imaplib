%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.09.2021
%%% @doc

-module(imaplib_utils_parse).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([parse/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

parse(Filepath) when is_list(Filepath) ->
    {ok,Data} = ?BLfile:read_file(Filepath),
    parse_data(Data,#{filepath => Filepath,
                      dirpath => filename:dirname(Filepath)}).

parse_data(Data,Map) when is_binary(Data), is_map(Map) ->
    parse_data_1(fix_headers(Data),Map).


parse_data_1(Data,Map) when is_binary(Data), is_map(Map) ->
    {_MimeType,_MimeSubtype,Headers0,_Parameters,_Body}=MimeTuple = mimemail:decode(Data),
    Headers = lowercase_headers(Headers0),
    Map1 = Map#{from => ?BU:get_by_key(<<"from">>,Headers),
                to => separate(?BU:get_by_key(<<"to">>,Headers)),
                cc => separate(?BU:get_by_key(<<"cc">>,Headers,<<>>)),
                bcc => separate(?BU:get_by_key(<<"bcc">>,Headers,<<>>)),
                subject => ?BU:get_by_key(<<"subject">>,Headers,<<>>),
                references => ?BU:get_by_key(<<"references">>,Headers,<<>>),
                native_id => ?BU:get_by_key(<<"message-id">>,Headers),
                in_reply_to => ?BU:get_by_key(<<"in-reply-to">>,Headers,<<>>),
                reply_to => ?BU:get_by_key(<<"reply-to">>,Headers,<<>>),
                native_datetime => case ?BU:get_by_key(<<"delivery-date">>,Headers,<<>>) of <<>> -> ?BU:get_by_key(<<"date">>,Headers,<<>>); D -> D end,
                size => size(Data)
              },
    parse_content(MimeTuple,Map1).

%% --------
%% @private
%% multipart/mixed, multipart/alternative, multipart/related
parse_content({<<"multipart">>,_,_,_,Body},Map) ->
    case Body of
        {_,_,_,_,B}=Part when is_binary(B) -> store_part(Part,Map);
        {<<"multipart">>,_,_,_,_}=Part -> parse_content(Part,Map);
        [Part] -> parse_content(Part,Map);
        [_,_|_]=Parts -> lists:foldl(fun(Part,{ok,MapF}) -> parse_content(Part,MapF) end, {ok,Map}, Parts)
    end;

%% other mimetypes
parse_content(MimeTuple,Map) ->
    store_part(MimeTuple,Map).

%% --------
%% @private
store_part({MimeType,MimeSubtype,Headers0,Parameters,Body}=MimeTuple, Map) ->
    Fdefault = fun() ->
                    case {MimeType,MimeSubtype} of
                        {<<"text">>,<<"plain">>} ->
                            {ok,Map#{text => Body}};
                        {<<"text">>,<<"html">>} ->
                            Filepath = do_write_file(".html",MimeTuple,Map),
                            {ok,Map#{html => Filepath}};
                        {<<"text">>,<<"calendar">>} ->
                            add_calendar_task(MimeTuple,Map);
                        _ ->
                            Filepath = do_write_file(MimeTuple,Map),
                            Others = maps:get(others,Map,[]),
                            {ok,Map#{others => [Filepath|Others]}}
                    end end,
    case ?BU:get_by_key(disposition,Parameters,undefined) of
        undefined -> Fdefault();
        <<"attachment">> ->
            % when multipart/mixed
            Filepath = do_write_file(MimeTuple,Map),
            Attachments = maps:get(attachments,Map,[]),
            {ok,Map#{attachments => lists:usort([Filepath|Attachments])}};
        <<"inline">> ->
            Headers = lowercase_headers(Headers0),
            case ?BU:get_by_key(<<"content-id">>,Headers,undefined) of
                ContentId when is_binary(ContentId) ->
                    % when multipart/related
                    Filepath = do_write_file(MimeTuple,Map),
                    Inlines = maps:get(inlines,Map,[]),
                    {ok,Map#{inlines => [{ContentId,Filepath}|Inlines]}};
                undefined ->
                    % when single part or general part of multipart/mixed and multipart/related.
                    % in multipart/alternative or not
                    Fdefault()
            end end.

%% @private
% 27.11.2023
% Add attachment instead of ics part
% Original:
%   Content-Type: text/calendar; charset=utf-8; method=REQUEST
%   Content-Transfer-Encoding: base64
% Target:
%   Content-Type: application/ics; charset=utf-8; name=event.ics
%   Content-Transfer-Encoding: base64
%   Content-Disposition: attachment; filename=event.ics
add_calendar_task({<<"text">>,<<"calendar">>,Headers0,Parameters,Body}=MimeTuple, Map) ->
    Fstore = fun(MapX,{Key,Value}) when is_map(MapX) -> MapX#{Key => Value};
                (ListX,{Key,Value}) when is_list(ListX) -> lists:keystore(Key,1,ListX,{Key,Value})
             end,
    CtParams = ?BU:get_by_key(content_type_params,Parameters,[]),
    case lists:keytake(<<"method">>,1,CtParams) of
        {value,{<<"method">>,Method},CtParams1} when Method==<<"REQUEST">> ->
            DispParams = ?BU:get_by_key(disposition_params,Parameters,[]),
            %
            CtParams2 = Fstore(CtParams1,{<<"name">>,<<"event.ics">>}),
            Parameters1 = Fstore(Parameters,{content_type_params,CtParams2}),
            Parameters2 = Fstore(Parameters1,{disposition,<<"attachment">>}),
            %
            DispParams1 = Fstore(DispParams,{<<"filename">>,<<"event.ics">>}),
            Parameters3 = Fstore(Parameters2,{disposition_params,DispParams1}),
            %
            MimeTuple1 = {<<"application">>,<<"ics">>,Headers0,Parameters3,Body},
            Filepath = do_write_file(MimeTuple1,Map),
            Attachments = maps:get(attachments,Map,[]),
            {ok,Map#{attachments => lists:usort([Filepath|Attachments])}};
        _ ->
            Filepath = do_write_file(MimeTuple,Map),
            Others = maps:get(others,Map,[]),
            {ok,Map#{others => [Filepath|Others]}}
    end.

%% @private
do_write_file(MimeTuple,Map) ->
    do_write_file(<<>>,MimeTuple,Map).
do_write_file(DefaultExtension,{_MimeType,_MimeSubtype,_Headers,Parameters,Body},Map) ->
    MsgFilepath = maps:get(filepath,Map),
    Dirpath = filename:dirname(MsgFilepath),
    Basename = filename:basename(MsgFilepath),
    Extension = filename:extension(Basename),
    SubFld = string:left(Basename,length(Basename) - length(Extension)) ++ ".files",
    %
    TmpFolder = filename:join(Dirpath,SubFld),
    ok = ?BLfilelib:ensure_dir(filename:join(TmpFolder,"x")),
    %
    DispParams = ?BU:get_by_key(disposition_params,Parameters,[]),
    CtParams = ?BU:get_by_key(content_type_params,Parameters,[]),
    %
    Filename = case ?BU:get_by_key(<<"filename">>,DispParams,undefined) of
                   Fn when is_binary(Fn) -> Fn;
                   undefined ->
                       case ?BU:get_by_key(<<"name">>,CtParams,undefined) of
                           Fn when is_binary(Fn) -> Fn;
                           undefined -> <<(?BU:newid())/binary,(?BU:to_binary(DefaultExtension))/binary>>
                       end end,
    Filepath = filename:join([TmpFolder,Filename]),
    ok = ?BLfile:write_file(Filepath,Body),
    Filepath.

%% @private
%% Fix multiline =?utf-8?Q?................?= spoiling utf8 line.
%% But non combine ?B? - base64 could be spoiled backwards.
%% Combine to one line when same encoding on all lines
fix_headers(Data) ->
    [Headers,Body] = binary:split(Data,<<"\r\n\r\n">>),
    Headers1 = binary:split(Headers,[<<"\r\n">>,<<"\n">>],[global]),
    Headers2 = lists:foldl(fun(<<" ",_/binary>>=H,[Last|Rest]) -> [Last ++ [trim_leading(H)] | Rest];
                              (<<"\t",_/binary>>=H,[Last|Rest]) -> [Last ++ [trim_leading(H)] | Rest];
                              (H,Acc) -> [N,V] = binary:split(H,<<":">>), [[N,trim_leading(V)] | Acc]
                           end, [], Headers1),
    Headers3 = lists:foldl(fun([H,V],Acc) ->
                                case check_header_name(H) of
                                    ok ->  [<<H/binary,": ",V/binary>>|Acc];
                                    error -> Acc
                                end;
                              ([H|_]=OneHeaderList,Acc) ->
                                  case check_header_name(H) of
                                      ok ->  [fix_header(OneHeaderList) | Acc];
                                      error -> Acc
                                  end
                           end, [], Headers2),
    Headers4 = lists:flatten(Headers3),
    Headers5 = ?BU:join_binary(Headers4, <<"\r\n">>),
    <<Headers5/binary,"\r\n\r\n",Body/binary>>.

%% @private
trim_leading(<<32,Rest/binary>>) -> trim_leading(Rest);
trim_leading(<<9,Rest/binary>>) -> trim_leading(Rest);
trim_leading(Value) -> Value.

%% @private
check_header_name(HeaderName) ->
    F = fun(X) -> X > 32 andalso X < 127 end,
    case binstr:all(F, HeaderName) of
        true -> ok;
        false -> error
    end.

%% @private
fix_header([H,V|Lines]) ->
    L1 = lists:map(fun(Line) ->
                        case re:run(Line, <<"=\\?(.*)\\?(.)\\?(.*)\\?=">>) of
                            nomatch -> Line;
                            {match,[{P0,C0},{P1,C1},{P2,C2},{P3,C3}]} ->
                                F = fun(From,Count) -> binary:part(Line,From,Count) end,
                                case {F(P1,C1), F(P2,C2)} of
                                    {Enc, Tr} when Tr==<<"Q">>; Tr==<<"q">> ->
                                        {F(0,P0), Enc, Tr, F(P3,C3), F(P0+C0,size(Line)-P0-C0)};
                                    {_, Tr} when Tr==<<"B">>; Tr==<<"b">> ->
                                        Line
                                end end end, [V|Lines]),
    R2 = lists:foldl(fun
                        % current is binary; previous is binary
                        (Line, {undefined,Acc}) when is_binary(Line) -> {undefined,[Line|Acc]};
                         % current is binary; previous is tuple
                        (Line, {{Begin,Enc,Tr,Content,<<>>},Acc}) when is_binary(Line) ->
                            {undefined,[Line,<<Begin/binary,"=?",Enc/binary,"?",Tr/binary,"?",Content/binary,"?=">> | Acc]};
                        % current is tuple ready to connect with next; previous is binary
                        ({_,_,_,_,<<>>}=Line, {undefined,Acc}) -> {Line,Acc};
                        % current is tuple not ready to connect with next lines; previous is binary
                        ({Begin1,Enc1,Tr1,Content1,End1}, {undefined,Acc}) ->
                            {undefined,[<<Begin1/binary,"=?",Enc1/binary,"?",Tr1/binary,"?",Content1/binary,"?=",End1/binary>> | Acc]};
                        % current is tuple ready to connect with previous line and next line; previous is tuple ready to connect with next
                        % tuples has same encoding
                        ({<<>>,Enc1,Tr1,Content1,<<>>}, {{Begin,Enc,Tr,Content,<<>>},Acc}) when Enc1==Enc, Tr1==Tr ->
                            {{Begin,Enc,Tr,<<Content/binary,Content1/binary>>,<<>>},Acc};
                        % current is tuple ready to connect with previous line and non ready to connect with next line; previous is tuple ready to connect with next
                        % tuples has same encoding
                        ({<<>>,Enc1,Tr1,Content1,End1}, {{Begin,Enc,Tr,Content,<<>>},Acc}) when Enc1==Enc, Tr1==Tr ->
                            {undefined, [<<Begin/binary,"=?",Enc/binary,"?",Tr/binary,"?",Content/binary,Content1/binary,"?=",End1/binary>>|Acc]};
                        % current is tuple ready to connect with next line; previous is tuple ready to connect with next
                        % tuples has different encodings
                        ({_,_,_,_,<<>>}=Line, {{Begin,Enc,Tr,Content,End},Acc}) ->
                            {Line,[<<Begin/binary,"=?",Enc/binary,"?",Tr/binary,"?",Content/binary,"?=",End/binary>>|Acc]};
                        % current is tuple not ready to connect with next line; previous is tuple ready to connect with next
                        % tuples has different encodings
                        ({Begin1,Enc1,Tr1,Content1,End1}, {{Begin,Enc,Tr,Content,End},Acc}) ->
                            {undefined,[<<Begin1/binary,"=?",Enc1/binary,"?",Tr1/binary,"?",Content1/binary,"?=",End1/binary>>,
                                        <<Begin/binary,"=?",Enc/binary,"?",Tr/binary,"?",Content/binary,"?=",End/binary>>
                                       |Acc]}
                     end, {undefined,[]}, L1),
    L3 = case R2 of
             {undefined,Acc} -> lists:reverse(Acc);
             {{Begin,Enc,Tr,Content,<<>>},Acc} -> lists:reverse([<<Begin/binary,"=?",Enc/binary,"?",Tr/binary,"?",Content/binary,"?=">> | Acc])
         end,
    [First|Rest] = lists:map(fun(Line) -> <<" ",Line/binary>> end, L3),
    [<<H/binary,":",First/binary>>|Rest].


%% ====================================================================
%% Internal functions
%% ====================================================================

lowercase_headers(Headers) ->
    lists:map(fun({N,V}) -> {?BU:lowercase(N),V} end, Headers).

separate(<<>>) -> [];
separate(Value) ->
    Items = binary:split(Value, [<<",">>,<<";">>], [global,trim_all]),
    lists:map(fun(Item) -> string:trim(Item,both,[32,9]) end, Items).

