%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.09.2021
%%% @doc

%% ====================================================================
%% Types
%% ====================================================================
                         
%% ====================================================================
%% Constants and defaults
%% ====================================================================

%% ====================================================================
%% Define modules
%% ====================================================================

-define(BASICLIB, basiclib).

-define(APP, imaplib).
-define(RoleName, "imaplib").

-define(SUPV, imaplib_supv).
-define(CFG, imaplib_config).

-define(Adapter, imaplib_utils_imap_adapter).
-define(Recv, imaplib_utils_recv).
-define(Parse, imaplib_utils_parse).
-define(List, imaplib_utils_list).

%% ------
%% From erlimap
%% ------
-define(IMAP, imap).

%% ------
%% From basiclib
%% ------
-define(BU, basiclib_utils).
-define(BLmulticall, basiclib_multicall).
-define(BLstore, basiclib_store).
-define(BLstore_template, basiclib_store_srv_template).
-define(BLping, basiclib_ping).
-define(BLmonitor, basiclib_monitor_srv).
-define(BLlog, basiclib_log).
-define(BLrpc, basiclib_rpc).
-define(BLfile, basiclib_wrapper_file).
-define(BLfilelib, basiclib_wrapper_filelib).

%% ====================================================================
%% Define logs
%% ====================================================================

-define(LOGFILE, {env,?APP}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), Text)).

-define(OUT(Level,Fmt,Args), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), Text)).
                             
